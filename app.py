# -*- coding: utf-8 -*-
"""
Created on 02/06/2021

@author: BEN SASSI Ousssema & ALOUADI Boutheina
"""

from __future__ import division, print_function

# import the necessary packages
from tensorflow.keras.preprocessing.image import img_to_array
import numpy as np
import argparse
import pickle
# coding=utf-8
import sys
import glob
import re
# Keras
from tensorflow.keras.preprocessing import image
# Flask utils
from flask_cors import CORS
from flask import jsonify
import json
import flask
from PIL import Image, ImageFile

from flask import Flask, render_template, request, session, redirect, url_for, flash
import os
from werkzeug.utils import secure_filename
from tensorflow.keras.models import load_model
#import matplotlib.pyplot as plt
import cv2



 
ImageFile.LOAD_TRUNCATED_IMAGES = True
UPLOAD_FOLDER = './flask app/assets/images'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])


# Create Database if it doesnt exist

app = Flask(__name__,static_url_path='/assets',
            static_folder='./flask app/assets', 
            template_folder='./flask app')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

MODEL_PATH3 = 'models/model_binaire_xray_nonxray.h5'
MODEL_PATH22 = 'models/model_NO_FINDING_2.h5'
MODEL_PATH2 = 'models/mlb_NO_FINDING_2.pickle'
MODEL_PATH = 'models/14_classes.h5'

#Load your trained model
model2 = load_model(MODEL_PATH22)
mlb = pickle.loads(open(MODEL_PATH2, "rb").read())
model3 = load_model(MODEL_PATH3)
model = load_model(MODEL_PATH)
#model._make_predict_function()         
print('Model loaded. Start serving...')



#Load Image
def model_predict2(img_path, model,size):
    img = image.load_img(img_path, target_size=size) 
    
    # Preprocessing the image
    img = image.img_to_array(img)
    img = np.expand_dims(img, axis=0)
    img=img/255
    preds = model2.predict(img)[0]
    idxs = np.argsort(preds)[::-1][:2]
    #print (preds)
    #print (idxs)
    return preds ,idxs


# FLASK PREDICTION
def model_predict(img_path, model,size):
    
    img = cv2.imread(img_path)
    img= cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = cv2.resize(img, (224, 224))
    img = img_to_array(img)
    img = np.expand_dims(img, axis=0)
    proba = model.predict(img)
    proba = proba.astype(float)*100
    return proba


@app.route('/')
def root():
   return render_template('index.html')

@app.route('/index.html')
def index():
   return render_template('index.html')

@app.route('/contact.html')
def contact():
   return render_template('contact.html')

@app.route('/news.html')
def news():
   return render_template('news.html')

@app.route('/about.html')
def about():
   return render_template('about.html')

@app.route('/faqs.html')
def faqs():
   return render_template('faqs.html')

@app.route('/prevention.html')
def prevention():
   return render_template('prevention.html')

@app.route('/upload.html')
def upload():
   return render_template('upload.html')

@app.route('/upload_chest.html')
def upload_chest():
   return render_template('upload_chest.html')


@app.route('/upload_ct.html')
def upload_ct():
   return render_template('upload_ct.html')

@app.route('/uploaded_chest', methods = ['POST', 'GET'])

def uploaded_chest():
    if flask.request.method == 'POST':
        # check if the post request has the file part
        f = flask.request.files['file']   
        basepath = os.path.dirname(__file__)
        file_path = os.path.join(basepath, app.config['UPLOAD_FOLDER'], 'upload_chest.jpg')        
        f.save(file_path)    

        # Make prediction
        proba = model_predict(file_path, model,(224,224))
        preds,idxs = model_predict2(file_path, model2,(96,96))
        print(preds)
        print(idxs)

        
        
        #print(proba.shape)
        maladie=['Atelectasis','Cardiomegaly','Consolidation', 'Edema', 'Effusion', 'Emphysema','Fibrosis', 'Infiltration', 'Mass', 'Nodule', 'Pleural_Thickening', 'Pneumonia', 'Pneumothorax']
        preds2 = model_predict(file_path, model3,(224,224))
        labels_str2 = ['xray', 'nonXray']
        listlabel=[]
        
        
        for (i, j) in enumerate(idxs):
            label = str(mlb.classes_[j])
            listlabel.append(label)

        #print(listlabel)    
        name='COVID'    
        reponse = "OK"
        
        #Statut of images xray or not
        if preds2[0] > 0.5:
            reponse= "BAD_IMAGE"
            data = {
                "prediction": {
                str(mlb.classes_[0]): "0",
                str(mlb.classes_[1]): "0",
                str(mlb.classes_[2]): "0",
                #str(mlb.classes_[3]): "0",
                str(mlb.classes_[4]): "0",
                #str(mlb.classes_[5]): "0",
                str(mlb.classes_[6]): "0",
                str(mlb.classes_[7]): "0",
                str(mlb.classes_[8]): "0",
                str(mlb.classes_[9]): "0",
                str(maladie[0]): "0", 
                str(maladie[1]): "0", 
                str(maladie[2]): "0", 
                str(maladie[3]): "0", 
                str(maladie[4]): "0", 
                str(maladie[5]): "0", 
                str(maladie[6]): "0",
                str(maladie[7]): "0", 
                str(maladie[8]): "0", 
                str(maladie[9]): "0", 
                str(maladie[10]): "0",
                str(maladie[11]): "0",
                str(maladie[12]): "0"

                },
                "decision":{
                "COVID_POSITIVE": "0"
                },
                "statut":{
                "code": reponse
                }
            }
        
            xxi= json.dumps(data,indent=4)
            xray_or_not=xxi
            #return xray_or_not 
            return render_template('results_chest.html',x=xray_or_not)

        elif (name in listlabel ):    
            data = {
                "prediction": {
                str(mlb.classes_[0]): str(round(preds[0] * 100)),
                str(mlb.classes_[1]): str(round(preds[1] * 100)),
                str(mlb.classes_[2]): str(round(preds[2] * 100)),
                #str(mlb.classes_[3]): str(round(preds[3] * 100)),
                str(mlb.classes_[4]): str(round(preds[4] * 100)),
                #str(mlb.classes_[5]): str(round(preds[5] * 100)),
                str(mlb.classes_[6]): str(round(preds[6] * 100)),
                str(mlb.classes_[7]): str(round(preds[7] * 100)),
                str(mlb.classes_[8]): str(round(preds[8])* 100),
                str(mlb.classes_[9]): str(round(preds[9])* 100),
                str(maladie[0]): str(round(proba[0][0])), 
                str(maladie[1]): str(round(proba[0][1])), 
                str(maladie[2]): str(round(proba[0][2])), 
                str(maladie[3]): str(round(proba[0][3])), 
                str(maladie[4]): str(round(proba[0][4])), 
                str(maladie[5]): str(round(proba[0][5])), 
                str(maladie[6]): str(round(proba[0][6])),
                str(maladie[7]): str(round(proba[0][7])), 
                str(maladie[8]): str(round(proba[0][8])), 
                str(maladie[9]): str(round(proba[0][9])), 
                str(maladie[10]): str(round(proba[0][10])),
                str(maladie[11]): str(round(proba[0][11])),
                str(maladie[12]): str(round(proba[0][12]))

                },
                "decision":{
                "COVID_POSITIVE": "1"
                },
                "statut":{
                "code": reponse
                }
            }                
            i= json.dumps(data,indent=4)
            mm=i
            return render_template('results_chest.html',x=mm)

        #COVID_POSITIVE =0    
        else :    
            data = {
                "prediction": {
                str(mlb.classes_[0]): str(round(preds[0] * 100)),
                str(mlb.classes_[1]): str(round(preds[1] * 100)),
                str(mlb.classes_[2]): str(round(preds[2] * 100)),
                #str(mlb.classes_[3]): str(round(preds[3] * 100)),
                str(mlb.classes_[4]): str(round(preds[4] * 100)),
                #str(mlb.classes_[5]): str(round(preds[5] * 100)),
                str(mlb.classes_[6]): str(round(preds[6] * 100)),
                str(mlb.classes_[7]): str(round(preds[7] * 100)),
                str(mlb.classes_[8]): str(round(preds[8])* 100),
                str(mlb.classes_[9]): str(round(preds[9])* 100),
                str(maladie[0]): str(round(proba[0][0])), 
                str(maladie[1]): str(round(proba[0][1])), 
                str(maladie[2]): str(round(proba[0][2])), 
                str(maladie[3]): str(round(proba[0][3])), 
                str(maladie[4]): str(round(proba[0][4])), 
                str(maladie[5]): str(round(proba[0][5])), 
                str(maladie[6]): str(round(proba[0][6])),
                str(maladie[7]): str(round(proba[0][7])), 
                str(maladie[8]): str(round(proba[0][8])), 
                str(maladie[9]): str(round(proba[0][9])), 
                str(maladie[10]): str(round(proba[0][10])),
                str(maladie[11]): str(round(proba[0][11])),
                str(maladie[12]): str(round(proba[0][12]))

                },
                "decision":{
                "COVID_POSITIVE": "0"
                },
                "statut":{
                "code": reponse
                }
            }
            i= json.dumps(data,indent=4)
            mmm= i
            return render_template('results_chest.html',x=mmm)
    return None

   

#@app.route('/uploaded_ct', methods = ['POST', 'GET'])
# def uploaded_ct():
   # if request.method == 'POST':
        # # check if the post request has the file part
        # if 'file' not in request.files:
            # flash('No file part')
            # return redirect(request.url)
        # file = request.files['file']
        # # if user does not select file, browser also
        # # submit a empty part without filename
        # if file.filename == '':
            # flash('No selected file')
            # return redirect(request.url)
        # if file:
            # # filename = secure_filename(file.filename)
            # file.save(os.path.join(app.config['UPLOAD_FOLDER'], 'upload_ct.jpg'))

   # resnet_ct = load_model('models/resnet_ct.h5')
   # vgg_ct = load_model('models/vgg_ct.h5')
   # inception_ct = load_model('models/inception_ct.h5')
   # xception_ct = load_model('models/xception_ct.h5')

   # image = cv2.imread('./flask app/assets/images/upload_ct.jpg') # read file 
   # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) # arrange format as per keras
   # image = cv2.resize(image,(224,224))
   # image = np.array(image) / 255
   # image = np.expand_dims(image, axis=0)
   
   # resnet_pred = resnet_ct.predict(image)
   # probability = resnet_pred[0]
   # print("Resnet Predictions:")
   # if probability[0] > 0.5:
      # resnet_ct_pred = str('%.2f' % (probability[0]*100) + '% COVID') 
   # else:
      # resnet_ct_pred = str('%.2f' % ((1-probability[0])*100) + '% NonCOVID')
   # print(resnet_ct_pred)

   # vgg_pred = vgg_ct.predict(image)
   # probability = vgg_pred[0]
   # print("VGG Predictions:")
   # if probability[0] > 0.5:
      # vgg_ct_pred = str('%.2f' % (probability[0]*100) + '% COVID') 
   # else:
      # vgg_ct_pred = str('%.2f' % ((1-probability[0])*100) + '% NonCOVID')
   # print(vgg_ct_pred)

   # inception_pred = inception_ct.predict(image)
   # probability = inception_pred[0]
   # print("Inception Predictions:")
   # if probability[0] > 0.5:
      # inception_ct_pred = str('%.2f' % (probability[0]*100) + '% COVID') 
   # else:
      # inception_ct_pred = str('%.2f' % ((1-probability[0])*100) + '% NonCOVID')
   # print(inception_ct_pred)

   # xception_pred = xception_ct.predict(image)
   # probability = xception_pred[0]
   # print("Xception Predictions:")
   # if probability[0] > 0.5:
      # xception_ct_pred = str('%.2f' % (probability[0]*100) + '% COVID') 
   # else:
      # xception_ct_pred = str('%.2f' % ((1-probability[0])*100) + '% NonCOVID')
   # print(xception_ct_pred)

   # return render_template('results_ct.html',resnet_ct_pred=resnet_ct_pred,vgg_ct_pred=vgg_ct_pred,inception_ct_pred=inception_ct_pred,xception_ct_pred=xception_ct_pred)

if __name__ == '__main__':
   app.secret_key = ".."
   app.run()